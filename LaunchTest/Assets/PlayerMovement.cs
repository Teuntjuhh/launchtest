﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public GameObject RubberBand;
    public float elasticity;
    public float elasticForce;
    public float speedModifier;

    public Vector2 touchStart;
    public Vector2 touchMovement;
    public float touchAngle;

    public float velocity;
    public float movementDirectionX;
    public float movementDirectionZ;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //transform.Translate(velocity * Time.deltaTime * new Vector3(movementDirection, 0, 0), Space.World);
        transform.position += velocity * Time.deltaTime * new Vector3(movementDirectionX, 0, movementDirectionZ);

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    touchStart = touch.position;
                    break;

                case TouchPhase.Moved:
                    touchMovement = touch.position - touchStart;
                    
                    if(touchMovement.x >= 0)
                    {
                        touchAngle = Mathf.Atan2(touchMovement.x, touchMovement.y) * Mathf.Rad2Deg;
                    }
                    else
                    {
                        touchAngle = 360 - Mathf.Atan2(touchMovement.x, touchMovement.y) * Mathf.Rad2Deg * -1;
                    }

                    transform.rotation = Quaternion.Euler(0, touchAngle - 180, 0);

                    RubberBand.transform.localScale = new Vector3(1, 1, touchMovement.magnitude * elasticity);
                    RubberBand.transform.localPosition = new Vector3(0, 0, touchMovement.magnitude * -0.5f * elasticity);
                    break;

                case TouchPhase.Ended:
                    velocity = touchMovement.magnitude * speedModifier;
                    movementDirectionX = transform.rotation.y;
                    movementDirectionZ = transform.rotation.y + 0.5f;
                    break;
            }
        }
        else
        {
            RubberBand.transform.localScale = Vector3.Lerp(RubberBand.transform.localScale, new Vector3(1, 1, 1), elasticForce);
            RubberBand.transform.localPosition = Vector3.Lerp(RubberBand.transform.localPosition, new Vector3(0, 0, 0), elasticForce);
        }
	}
}
